

 class PersonClass {
    constructor(personName, personAge, gender) {
        this.personName = personName;
        this.personAge = personAge;
        this.gender = gender;
    }
    getPersonInfo() {
        return "PersonName: " + this.personName + " " + "PersonAge: " + " " + this.personAge + " " + "Gender: " + this.gender;
    }
}
export { PersonClass }