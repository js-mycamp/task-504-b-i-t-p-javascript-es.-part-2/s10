import { PersonClass } from "./personclass.js";
class employeeClass extends PersonClass {
    constructor(personName, personAge, gender,employer,salary,position) {
        super(personName, personAge, gender)
        this.employer = employer;
        this.salary = salary;
        this.position = position;
    }
    getEmployeInfo(){
        console.log("Employe Name: " + this.personName);
        console.log("Employe Age: " + this.personAge);
        console.log("Gender of Employe: " + this.gender);
        console.log("employer : " + this.employer);
        console.log("salary : " + this.salary);
        console.log("position : " + this.position);
    }
    getSalary(){
        console.log(this.salary) 
    }
    setSalary(newSalary) {
        newSalary = newSalary.trim();
        if (newSalary === '') {
            throw 'The Salary cannot be empty';
        }
        this.Salary = newSalary;
    }
    setPosition(newPosition) {
        newPosition = newPosition.trim();
        if (newPosition === '') {
            throw 'The Position cannot be empty';
        }
        this.position = newPosition;
    }
    getPosition(){
        console.log(this.position) 
    }

}
export {employeeClass}