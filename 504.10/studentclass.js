
import { PersonClass } from "./personclass.js";     
    class StudentClass extends PersonClass {
        constructor(personName, personAge, gender, standard, collegeName, grade) {
            super(personName, personAge, gender)
            this.standard = standard;
            this.collegeName = collegeName;
            this.grade = grade;
        }
        getStudentInfo() {
            console.log("StudentName: " + this.personName);
            console.log("StudentAge: " + this.personAge);
            console.log("Gender of Student: " + this.gender);
            console.log("Student standard: " + this.standard);
            console.log("CollegeName of Student: " + this.collegeName);
            console.log("Student grade: " + this.grade);
        }
        getGrade(){
            console.log(this.grade);
        }
        setGrade(newGrade) {
        newGrade = newGrade.trim();
        if (newGrade === '') {
            throw 'The Grade cannot be empty';
        }
        this.grade = newGrade;
    }

    }
    
    export {StudentClass}